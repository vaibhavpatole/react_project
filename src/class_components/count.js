import React, { Component } from 'react'

export class count extends Component {
  
  constructor(props) {
    super(props)
  
    this.state = {
        timer:0
    }

    this.increment = this.increment.bind(this)
  }

  increment(){
    //set the variable value using corrent value
    // this.setState({
    //   timer : this.state.timer + 1
    // })

    //set the value using previous value
    this.setState((prevState)=>({
      timer :prevState.timer + 1
    }))
  }

  // increment = ()=>{
  //   this.setState((prevState)=>({
  //     timer :prevState.timer + 1
  //   }))
  // }
  

  render() {
    return (
      <div>
        <p>counter : {this.state.timer}</p>
        {/* <button onClick ={()=>{this.increment()}}>click </button> */}
        {/* <button onClick ={()=>{this.increment.bind(this)}}>click </button> */}
        <button onClick ={this.increment}>click </button>
      </div>
    )
  }
}

export default count
