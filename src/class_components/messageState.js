import React,{ Component } from 'react';

class messageState extends Component{

	constructor(){
		super()
		this.state = {
			message : 'message chnage'
		}
	}

	changeMessage(text1){
		this.setState({
			message : text1
		})
	}

  render(){
    return (
			<div>
				<h1>{ this.state.message }</h1>
				<button onClick ={()=> this.changeMessage('vabs clicked the button')}>click Me</button>
			</div>
		)
  }
}

export default messageState