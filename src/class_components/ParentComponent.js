import React, { Component } from 'react'
import ChildComponent from '../components/ChildComponent'

class ParentComponent extends Component {

  constructor(){
		super()
		this.state = {
			message : 'vaibhav'
    }
    this.changeMessage = this.changeMessage.bind(this)
	}

	changeMessage(){
		alert(`welcome ${this.state.message}`)
	}

  render() {
    return (
      <div>
        <ChildComponent  parentMethod = { this.changeMessage } />
      </div>
    )
  }
}

export default ParentComponent
