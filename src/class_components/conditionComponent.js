import React, { Component } from 'react'

class conditionComponent extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
       isLoggedIn:false
    }
  }
  

  render() {
    return this.state.isLoggedIn ? (<div>
        <h3>Welcome Vabs</h3>
      </div>):(<div>
        <h3>Welcome Guest</h3>
      </div>)  
  }
}

export default conditionComponent
