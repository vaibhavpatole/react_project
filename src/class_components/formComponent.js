import React, { Component } from 'react'

export class formComponent extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
       email:'',
       password:''
    }
  }

  chngeEmailId =(event) =>{
    this.setState({
      email:event.target.value
    })
  }

  chngePassword =(event) =>{
    this.setState({
      password:event.target.value
    })
  }

  submitValue = () => {
    alert(`email id is ${this.state.email} and password is ${this.state.password}`)
  }

  render() {
    return (
      <form onSubmit ={this.submitValue}>  
        <div>
          <label>Email Id </label>
          <input type="text" value={this.state.email} onChange={this.chngeEmailId} />
        </div>
        <div>
          <label>Password </label>
          <input type="password" value={this.state.password} onChange={this.chngePassword} />
        </div>
        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
    )
  }
}

export default formComponent
