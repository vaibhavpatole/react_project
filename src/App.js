import React from 'react';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Greet from './components/Greet';
import Welcome from './class_components/welcome';
import MessageState from './class_components/messageState';
import Count from './class_components/count';
import ParentComponent from './class_components/ParentComponent';
import Condit from './class_components/conditionComponent';
import NameList from './components/NameList';
import Form from './class_components/formComponent';

function App() {
  return (
    <div className="App">
      <Header  />
      {/* <Greet name="vaibhav patole"/>
      <Welcome name = "vabs" />
      <MessageState />
      <Count /> 
      <ParentComponent /> 
      <Condit /> 
      <NameList /> */}
      <Form />
      <Footer  email="vabs@gmail.com" />
    </div>
  );
}

export default App;
