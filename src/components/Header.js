import React from 'react';
import './Header.css';
import logo from '../../src/logo.svg';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const Header = ()=>
  <header className="App-header">
    <img src={logo} className="Header-logo" alt="logo" />
    <a className="Header-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">Learn React</a>
    <AccountCircleIcon style={{ fontSize:50,margin:20 }} />
  </header>

export default Header;