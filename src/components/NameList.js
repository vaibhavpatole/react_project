import React from 'react'

function NameList() {

  const person = [{name:"vaibhav",age:23},{name:"rohan",age:24}]

  const personList = person.map(person => 
      <h3>I am {person.name}.My age is {person.age}</h3>
  )

  return (
    <div>
      {personList}
    </div>
  )
}

export default NameList



