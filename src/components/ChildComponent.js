import React from 'react'

function ChildComponent(props) {
  return (
    <div>
      <button onClick={props.parentMethod}>Click</button>
    </div>
  )
}

export default ChildComponent
